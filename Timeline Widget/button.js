// Add UI Button shortcut
api.addButtonToToolbar({
    title: 'Timeline',
    icon: 'bx bxs-calendar',
    action: async () => api.activateNote(await api.startNote.getRelationValue('targetNote'))
});
